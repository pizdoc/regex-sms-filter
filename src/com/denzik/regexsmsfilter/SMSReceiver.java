package com.denzik.regexsmsfilter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {

	private String mSender;
	private String mBody = "";

	@Override
	public void onReceive(Context context, Intent intent) {

		Bundle bundle = intent.getExtras();
		Object[] pdus = (Object[]) bundle.get("pdus");

		for (int i = 0; i < pdus.length; i++) {
			SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[i]);
			mBody += sms.getMessageBody();
		}

		SmsMessage first = SmsMessage.createFromPdu((byte[]) pdus[0]);
		mSender = first.getOriginatingAddress();

		Filter mFilter = new Filter(context);
		
		if (mFilter.isSpam(mSender, mBody)) {
			
			DatabaseHelper dbHelper = new DatabaseHelper(context);
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			
			dbHelper.addMessage(mSender, mBody, db);
			
			dbHelper.close();
			abortBroadcast();
		}
	}

}
