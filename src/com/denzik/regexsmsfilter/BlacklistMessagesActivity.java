package com.denzik.regexsmsfilter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class BlacklistMessagesActivity extends Activity {

	final static String BODY = "body";
	final static String DATE_RECEIVED = "date_received";
	private String date_received = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blacklist_messages);
		
		Intent intent = getIntent();
		String sender = intent.getStringExtra("sender");

		DatabaseHelper dbHelper = new DatabaseHelper(this);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = dbHelper.messages(sender, db);

		setTitle(getString(R.string.blacklist_messages_title) + " " + sender + " (" + cursor.getCount() + ")");

		ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(cursor.getCount());
		Map<String, Object> m;

		if (cursor.moveToFirst()) {

			do {
				
				try {
					date_received = formatToYesterdayOrToday(cursor.getString(cursor.getColumnIndex(DATE_RECEIVED)));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				m = new HashMap<String, Object>();
				m.put(BODY, cursor.getString(cursor.getColumnIndex(BODY)));
				m.put(DATE_RECEIVED, date_received);
				data.add(m);

			} while (cursor.moveToNext());

			String[] from = { BODY, DATE_RECEIVED };
			int[] to = { R.id.tvMsgBody, R.id.tvMsgDate };

			SimpleAdapter adapter = new SimpleAdapter(this, data, R.layout.list_messages, from, to);
			final ListView lv = (ListView) findViewById(R.id.listViewMessages);
			lv.setAdapter(adapter);
			lv.post(new Runnable() {

				@Override
				public void run() {
					lv.setSelection(lv.getCount() - 1);
				}

			});
		}

		cursor.close();
		dbHelper.close();
	}

	/**
	 * Конверт даты Если сообщение получено сегодня/вчера, вернет текст сегодня/вчера + время
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public String formatToYesterdayOrToday(String date) throws ParseException {
		Date dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
		SimpleDateFormat outFormat = new SimpleDateFormat("EEE, d MMM, HH:mm");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateTime);
		Calendar today = Calendar.getInstance();
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");

		if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
			return getString(R.string.blacklist_date_today) + ", " + timeFormatter.format(dateTime);
		} else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
			return getString(R.string.blacklist_date_yesterday) + ", " + timeFormatter.format(dateTime);
		} else {
			return outFormat.format(dateTime);
		}
	}

}
