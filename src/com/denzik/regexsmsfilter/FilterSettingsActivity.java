package com.denzik.regexsmsfilter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class FilterSettingsActivity extends Activity {

	private EditText mRegex;
	private EditText mWhatFind;
	private Spinner mSpinner;
	private PrefsHelper mPrefs;
	private int stateSpinner;
	private String mWhatFindString;
	private String mRegexString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filter_settings);

		mPrefs = new PrefsHelper(this);

		mWhatFind = (EditText) findViewById(R.id.what_find);
		mRegex = (EditText) findViewById(R.id.regex);
		mRegex.setText(mPrefs.getRegex());

		mSpinner = (Spinner) findViewById(R.id.where_find);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.where, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinner.setAdapter(adapter);
		mSpinner.setPrompt(getString(R.string.spinner_prompt));
		mSpinner.setSelection(mPrefs.getStateSpinner());
		mSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				stateSpinner = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

	}

	/**
	 * Проверить регулярное выражение
	 * 
	 * @param view
	 */
	public void onClick_check_regex(View view) {

		Filter mFilter = new Filter(this);

		mRegexString = mRegex.getText().toString().trim();
		mWhatFindString = mWhatFind.getText().toString().trim();

		if (mFilter.checkRegex(mRegexString, mWhatFindString)) {
			Toast.makeText(this, R.string.toast_what_find_true, Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, R.string.toast_what_find_false, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Сохранить настройки
	 * 
	 * @param view
	 */
	public void onClick_save_settings(View view) {

		mRegexString = mRegex.getText().toString().trim();

		if (mRegexString != null && mRegexString.length() > 0) {

			mPrefs.setRegex(mRegexString);
			mPrefs.setStateSpinner(stateSpinner);

			Toast.makeText(this, R.string.toast_save_settings, Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, R.string.toast_save_settings_error, Toast.LENGTH_SHORT).show();
		}
	}

}
