package com.denzik.regexsmsfilter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

public class Filter {

	private Context mContext;
	private ComponentName mComponent;
	private Pattern p;
	private Matcher m;

	public Filter(Context context) {
		mContext = context;
		mComponent = new ComponentName(context, SMSReceiver.class);
	}

	/**
	 * Проверяет, включен ли фильтр
	 * 
	 * @return boolean
	 */
	public boolean isEnabled() {
		int status = mContext.getPackageManager().getComponentEnabledSetting(mComponent);
		if (status == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) {
			return true;
		}
		return false;
	}

	/**
	 * Включить фильтр
	 */
	public void on() {
		if (!isEnabled()) {
			mContext.getPackageManager().setComponentEnabledSetting(mComponent, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
		} else {
			Toast.makeText(mContext, R.string.toast_already_on, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Выключает фильтр
	 */
	public void off() {
		if (isEnabled()) {
			mContext.getPackageManager().setComponentEnabledSetting(mComponent, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
		} else {
			Toast.makeText(mContext, R.string.toast_already_off, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Проверяет, является ли сообщение спамом
	 * 
	 * @param sender Имя или номер отправителя
	 * @param message Тело сообщения
	 * @return
	 */
	public boolean isSpam(String sender, String message) {

		PrefsHelper mPrefs = new PrefsHelper(mContext);

		String regex = mPrefs.getRegex();
		String text;
		int stateSpinner = mPrefs.getStateSpinner();

		if (regex != null) {

			switch (stateSpinner) {
				case 1:
					text = sender;
					break;
				case 2:
					text = message;
					break;
				default:
					text = sender + " " + message;
			}

			return checkRegex(regex, text);
		}
		return false;
	}

	/**
	 * Проверка регулярного выражения
	 * 
	 * @param regex Регулярное выражение
	 * @param text Текст в котором ищем
	 * @return boolean
	 */
	public boolean checkRegex(String regex, String text) {

		if (regex != null && text != null) {

			if (regex.length() > 0 && text.length() > 0) {

				p = Pattern.compile(regex);
				m = p.matcher(text);
				if (m.find()) {
					return true;
				}
			}
		}
		return false;
	}
}
