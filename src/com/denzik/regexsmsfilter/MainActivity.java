package com.denzik.regexsmsfilter;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	private Filter mFilter;
	private ToggleButton tb;
	private String regex;
	private PrefsHelper mPrefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	protected void onResume() {
		super.onResume();

		mFilter = new Filter(this);
		mPrefs = new PrefsHelper(this);

		regex = mPrefs.getRegex();

		tb = (ToggleButton) findViewById(R.id.filter_switcher);
		tb.setChecked(mFilter.isEnabled());

		DatabaseHelper dbHelper = new DatabaseHelper(this);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String count = String.valueOf(dbHelper.countAll(db));

		Button bb = (Button) findViewById(R.id.blacklist);
		bb.setText(getString(R.string.btn_blacklist) + " (" + count + ")");

		dbHelper.close();
	}

	/**
	 * Переход к настройкам фильтра
	 * 
	 * @param view
	 */
	public void onClick_filter_settings(View view) {
		Intent intent = new Intent(this, FilterSettingsActivity.class);
		startActivity(intent);
	}

	/**
	 * Переход к черному списку
	 * 
	 * @param view
	 */
	public void onClick_blacklist(View view) {
		Intent intent = new Intent(this, BlacklistSendersActivity.class);
		startActivity(intent);
	}

	/**
	 * Включить/Выключить фильтр
	 * 
	 * @param view
	 */
	public void onClick_filter_switcher(View view) {
		if (tb.isChecked()) {

			if (regex != null) {
				mFilter.on();
			} else {
				tb.setChecked(false);
				Toast.makeText(this, R.string.toast_not_set, Toast.LENGTH_SHORT).show();
			}

		} else {
			mFilter.off();
		}
	}
}
