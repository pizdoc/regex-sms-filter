package com.denzik.regexsmsfilter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class BlacklistSendersActivity extends Activity {

	final static String SENDER = "sender";
	final static String BODY = "body";
	final static String DATE_RECEIVED = "date_received";
	final static String COUNT = "_count";
	final int DIALOG_CLEAR = 1;
	private long count;
	private String date_received = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blacklist_senders);
	}

	@Override
	protected void onResume() {
		super.onResume();

		DatabaseHelper dbHelper = new DatabaseHelper(this);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		count = dbHelper.countAll(db);

		setTitle(getString(R.string.blacklist_senders_title) + " (" + count + ")");

		Cursor cursor = dbHelper.senders(db);

		ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(cursor.getCount());
		Map<String, Object> m;

		if (cursor.moveToFirst()) {

			TextView tvNoData = (TextView) findViewById(android.R.id.empty);
			tvNoData.setVisibility(View.INVISIBLE);

			do {

				try {
					date_received = formatToYesterdayOrToday(cursor.getString(cursor.getColumnIndex(DATE_RECEIVED)));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				m = new HashMap<String, Object>();
				m.put(SENDER, cursor.getString(cursor.getColumnIndex(SENDER)));
				m.put(BODY, cursor.getString(cursor.getColumnIndex(BODY)));
				m.put(DATE_RECEIVED, date_received);
				m.put(COUNT, " (" + cursor.getString(cursor.getColumnIndex(COUNT)) + ")");
				data.add(m);

			} while (cursor.moveToNext());

			String[] from = { SENDER, BODY, DATE_RECEIVED, COUNT };
			int[] to = { R.id.tvSender, R.id.tvBody, R.id.tvDate, R.id.tvCount };

			SimpleAdapter adapter = new SimpleAdapter(this, data, R.layout.list_senders, from, to);
			ListView lv = (ListView) findViewById(R.id.listViewSenders);
			lv.setAdapter(adapter);
			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					TextView tv = (TextView) view.findViewById(R.id.tvSender);
					Intent intent = new Intent(BlacklistSendersActivity.this, BlacklistMessagesActivity.class);
					intent.putExtra("sender", tv.getText().toString());
					startActivity(intent);
				}
			});
		}
		cursor.close();
		dbHelper.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, R.string.blacklist_senders_menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == 1) {
			if (count > 0) {

				AlertDialog.Builder ad = new AlertDialog.Builder(this);
				ad.setTitle(R.string.dialog_clear_title);
				ad.setIcon(android.R.drawable.ic_dialog_alert);
				ad.setMessage(R.string.dialog_clear_message);
				ad.setPositiveButton(R.string.dialog_clear_yes, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int arg1) {

						DatabaseHelper dbHelper = new DatabaseHelper(BlacklistSendersActivity.this);
						SQLiteDatabase db = dbHelper.getWritableDatabase();

						dbHelper.clear(db);
						dbHelper.close();

						finish();
						startActivity(getIntent());
					}

				});
				ad.setNegativeButton(R.string.dialog_clear_no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});

				ad.show();

			}
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Конверт даты Если сообщение получено сегодня/вчера, вернет текст сегодня/вчера + время
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public String formatToYesterdayOrToday(String date) throws ParseException {
		Date dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
		SimpleDateFormat outFormat = new SimpleDateFormat("EEE, d MMM");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateTime);
		Calendar today = Calendar.getInstance();
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");

		if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
			return getString(R.string.blacklist_date_today) + ", " + timeFormatter.format(dateTime);
		} else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
			return getString(R.string.blacklist_date_yesterday) + ", " + timeFormatter.format(dateTime);
		} else {
			return outFormat.format(dateTime);
		}
	}

}
