package com.denzik.regexsmsfilter;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsHelper {

	private static final String PREFS_NAME = "regexsmsfilter_prefs";
	private SharedPreferences mPrefs;
	private SharedPreferences.Editor e;

	public PrefsHelper(Context context) {
		mPrefs = context.getSharedPreferences(PREFS_NAME, 0);
	}

	public String getRegex() {
		return mPrefs.getString("regex", null);
	}

	public void setRegex(String value) {
		e = mPrefs.edit();
		e.putString("regex", value);
		e.commit();
	}

	public int getStateSpinner() {
		return mPrefs.getInt("state_spinner", 0);
	}

	public void setStateSpinner(int value) {
		e = mPrefs.edit();
		e.putInt("state_spinner", value);
		e.commit();
	}

}
