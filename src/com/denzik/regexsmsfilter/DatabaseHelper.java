package com.denzik.regexsmsfilter;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	final static int DB_VERSION = 1;
	final static String DB_NAME = "regexsmsfilter.db";
	final static String TABLE_NAME = "blacklist";
	final static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + "_id integer primary key autoincrement, " + "sender text not null, "
			+ "body text not null, " + "date_received date)";

	final static String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DROP_TABLE);
		onCreate(db);
	}

	/**
	 * Кол-во записей в таблице
	 * @param db
	 * @return
	 */
	public long countAll(SQLiteDatabase db) {
		return DatabaseUtils.queryNumEntries(db, TABLE_NAME);
	}

	/**
	 * Очистка таблицы
	 * @param db
	 */
	public void clear(SQLiteDatabase db) {
		db.delete(TABLE_NAME, null, null);
	}

	/**
	 * Список отправителей, последнее сообщение, дата, кол-во сообщений от отправителя
	 * @param db
	 * @return
	 */
	public Cursor senders(SQLiteDatabase db) {
		return db.rawQuery("SELECT *, COUNT(*) as _count FROM (SELECT * FROM " + TABLE_NAME
				+ " ORDER BY date_received) GROUP BY sender ORDER BY date_received DESC", null);
	}

	/**
	 * Все сообщения отправителя
	 * @param sender
	 * @param db
	 * @return
	 */
	public Cursor messages(String sender, SQLiteDatabase db) {
		return db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE sender = '" + sender + "'", null);
	}

	/**
	 * Добавить сообщение в бд
	 * @param sender
	 * @param body
	 * @param db
	 */
	public void addMessage(String sender, String body, SQLiteDatabase db) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();

		ContentValues cv = new ContentValues();
		cv.put("sender", sender);
		cv.put("body", body);
		cv.put("date_received", dateFormat.format(date));
		db.insert(TABLE_NAME, null, cv);
	}

}
